# iTunes Search

<p align="center">
    <img src="images/Screenshot.png">
</p>

## iTunes Search built with [Vue.js](https://vuejs.org/)

### [Demo](http://itunes-search.romanpaprotsky.com/)

### [YouTube](https://www.youtube.com/watch?v=TWArLk34Gl0)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
